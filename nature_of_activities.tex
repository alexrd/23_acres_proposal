\vspace{-3mm}
\section{Nature of Student Activities}
\label{sec:nature}
\vspace{-2mm}

% \red{Proposals should address the approach to undergraduate research training being taken and should provide detailed descriptions of examples of research projects that students will pursue. So that reviewers can evaluate intellectual merit, this discussion should indicate the significance of the research area and, when appropriate, the underlying theoretical framework, hypotheses, research questions, etc. Undergraduate research experiences have their greatest impact in situations that lead the students from a relatively dependent status to as independent a status as their competence warrants. Proposals must present plans that will ensure the development of student-faculty interaction and student-student communication. Development of collegial relationships and interactions is an important part of the project.}

The ACRES REU combines several distinct components: an authentic and meaningful research experience in computational or data science; workshops in technical skills; and professional development training that enhances their summer experience and informs their post-REU careers.  The capstone of the REU is participation in \href{https://urca.msu.edu/mid-sure}{the Mid-Michigan Symposium for Undergraduate Research (Mid-SURE)}, a state-wide poster session. In addition, students participate in the ACRES REU Annual Symposium talk series, presented to members of the REU, their mentors, and other members of the CMSE and ICER communities, which allow the students to synthesize and demonstrate mastery of their research project as well as the professional skills they have developed.

%to impart to them a general set of technical skills relating to computationally-focused research, and to provide them with professional development that enhances their summer experience, informs them about career and graduate school opportunities, and prepares them to be part of the 21st century workforce.

\vspace{1mm}
\noindent
\textbf{Research projects:} Each student will participate in an authentic and meaningful research experience in computational or data science, which may focus on algorithm development, scientific applications, or a combination of the two. A specific goal of each research project will be for students to learn foundational knowledge about their subject domain and relate this to the computational models and algorithms used in their project.  In the following pages we list potential research projects in a range of scientific areas and from a diverse set of mentors, all of whom are MSU faculty (and senior participants on this proposal) who have committed to mentoring students for at least one summer of the REU:

\vspace{1mm}
\noindent
\textbf{Project \#1: Modeling genetic interactions across species} (Computational biology)\\
Mentor: \href{https://shiulab.github.io}{Shinhan Shiu}, MSU Depts. of Plant Biology and CMSE \\
Description: The complexity of biological system is a consequence of both additive effects of thousands to tens of thousands of genes as well as their non-additive interactions (i.e., genetic interactions). We have substantial understanding of the additive effects of genes in influencing molecular behaviors of genes: traits such as crop yields and skin colors, and disease states such as diabetes and cancer. However, we currently have an incomplete understanding of how genes work with each other to impact biological systems, traits, and disease due to the challenges in assessing genetic interactions through experiments. In addition, experimental data on genetic interactions are mostly available from a few model species such as budding yeast, worm, fruit fly, and mouse ear cress. Thus, the student researchers will work with their mentors to collect a wide range of biological data from these species and use them to establish machine learning models for predicting genetic interactions within each of the species and between species.   \\
Representative publications:  \cite{shiu1,shiu2}

\vspace{1mm}
\noindent
\textbf{Project \#2: Open-source battery simulation} (Electrochemistry)\\
Mentor: \href{https://www.egr.msu.edu/people/profile/hcy}{Hui-Chia Yu}, MSU Dept. of CMSE \\
Description:  Optimizing electrode microstructures can push batteries' performance to their limits. The Yu group has developed an image-based simulation toolkit, which solves ion diffusion, electrical current continuity, and Arrhenius reactions to elucidate the electrochemical processes occurring in real microstructures. An outstanding need in this project is a driver code that alters microstructures and runs our current toolkit to autonomously search for the optimal electrode structures for fast charging. The REU students will participate the development of this driver code and simulation-based optimization design of batteries.\\
Representative publications: \cite{yu1,yu2}

\vspace{1mm}
\noindent
\textbf{Project \#3: Biomolecular aggregation and phase separation} (Computational biophysics)\\
Mentor: \href{http://feiglab.org}{Michael Feig}, MSU Dept. of Biochemistry \& Molecular Biology\\
Description: Biological macromolecules such as proteins and nucleic acids experience highly crowded environments inside cells. Using computer simulations one can analyze the molecular interactions under such conditions. Of particular interest is the propensity of molecules to aggregate and form phase-separated condensates. In this project the student researcher will use coarse-grained modeling tools such as COCOMO together with atomistic simulations to explore the determinants of aggregation or condensation and develop hypotheses that can be tested by experimental collaborators.\\
Representative publications: \cite{feig1,feig2}

\vspace{1mm}
\noindent
\textbf{Project \#4: Automated Image Annotation Toolkit} (Image analysis)\\
Mentor: \href{https://see-insight.github.io/}{Dirk Colbry}, Dept. of CMSE \\
Description: Advancements in imaging technology has led to the widespread use of image data in research across many scientific disciplines such as self-driving vehicles, medical research, and agriculture. However, manual processing and retrieval of specific information from scientific image data can be time-consuming and burdensome for researchers. In this project the student researcher will join the SEE-Insight team to research and develop a general purpose ``researcher in the loop'' toolkit that uses large scale computing to search the algorithm space for automated solutions to domain specific image annotation problems. These tools will speed up scientific discovery across disciplines and reduce the ``mean time to science''.
Representative publications: \cite{colbry1,colbry2}

\vspace{1mm}
\noindent
\textbf{Project \#5: Precision Surveillance Scheduling for Low-Grade Glioma Patients} (Medical imaging)\\
Mentor: \href{https://www.midilab.org/}{Adam Alessio}, Depts. of CMSE, Biomedical Engineering and Radiology \\
Description: Isocitrate dehydrogenase (IDH)-mutant gliomas are a classification of brain tumors that are understudied, incurable, and often impact young adult patients; yet clinical guidelines for monitoring use a one-size-fits-all surveillance schedule that is not optimized toward an individual's disease nor established treatment paradigms.  For example, patients with these gliomas receive MRI exams every 3-6 months which is then typically extended to longer intervals, and there are no established parameters to refine this approach which can be costly in the setting of delayed recurrence.  This work seeks to determine if clinical information, laboratory biomarkers, and MRI images can be used to predict tumor progression in order to customize the surveillance schedule for each patient.  In this project, the student researcher will use previously proposed deep-learning methods (based on 3D Convolutional Neural Networks) to develop a method to automatically segment gliomas and normal reference regions in the brain.  These segmentations will be used to extract radiomic features in the 3D glioma region and in the periphery (margin) of the gliomas.  This project benefits from exposure to an interdisciplinary medical research team and the ability to contribute to an ongoing pressing challenge in oncologic care.\\
Representative publications: \cite{alessio1}

\vspace{1mm}
\noindent
\textbf{Project \#6: Transport Phenomena in Warm Dense Matter Mixtures} (Plasma physics)\\
Mentor: \href{https://murillogroupmsu.com/}{Luciano Silvestri}, Dept. of CMSE \\
Description:  Warm Dense Matter (WDM) occupies a transitional state  between solid condensed matter and classical plasmas. It is characterized by conditions in which electrons are partially degenerate and ions can  be strongly correlated. This means that, in WDM, the electrons are hot enough that they begin to fill their quantum states above the Fermi energy, while the ions, though highly energetic, can still experience strong interactions with one another due to their close proximity.  WDM conditions are found in various astrophysical scenarios, such as the interiors of giant gas planets like Jupiter and in the late stages of stars like neutron stars and white dwarfs. On Earth, these conditions can be created in laboratory experiments, often involving intense laser or pulsed power facilities.  Given the challenging experimental conditions for producing and diagnosing WDM, researchers must rely on computer simulations. In particular, Molecular dynamics (MD) simulations are a crucial tool for studying Warm Dense Matter because they offer a detailed  atomic-level understanding. In this project students will perform molecular dynamics (MD) simulations to study transport phenomena and dynamical properties of WDM mixtures. \\
Representative publications: \cite{silvestri1,silvestri2}

\vspace{1mm}
\noindent
\textbf{Project \#7:  Vocal communication systems in animals} (Auditory processing)\\
Mentor: \href{https://smith-vidaurre.com/}{Grace Smith-Vidaurre}, Depts. of Integrative Biology and CMSE \\
Description:  Mammalian and avian species can learn their vocalizations in ways that are similar to humans. Learning vocalizations may be critical for animals to change the information that they communicate in different social contexts. However, addressing whether and how animals change learned vocalizations poses great challenges. Vocal learning species can be highly mobile, social animals that are difficult to mark, track, and record over time and across different social contexts. Acoustic recording datasets obtained from wild populations are often small and incomplete, with little information about individual identity, social group membership, sex, kinship, and behavioral context. We combine simulation and machine learning approaches to test how small, messy datasets can be used to make biological inferences about vocal communication systems that rely on social learning. REU students will simulate acoustic datasets, quantify patterns of acoustic variation, and test how biological inferences about vocal communication systems are robust to different sampling regimes and forms of missing data. \\
Representative publications: \cite{smith1,smith2}

\vspace{1mm}
\noindent
\textbf{Project \#8:  Directional Transform of 3D Shapes} (Topological Data Analysis)\\
Mentor: \href{https://elizabethmunch.com/}{Liz Munch}, Depts. of CMSE and Mathematics\\
Description: In order to measure shape, the field of Topological Data Analysis (TDA) encodes structure using tools from algebraic topology. The directional transform is a way to encode a particular shape in 3D by studying a function defined with levelsets normal to a chosen direction. Then we can use tools from TDA, such as the Euler characteristic or persistence diagram, to encode information about this particular function. In this project, students will study the directional transform of example data sets such as those arising from X-Ray CT scans of plants and realizations of attractors of dynamical systems. \\
Representative publications: \cite{munch1, munch2}

\vspace{1mm}
\noindent
\textbf{Project \#9: Mapping Emerging Trends and Interdisciplinary Links in Astrophysics Research with AI} (Metascience)\\
Mentor: \href{https://kerzendorf-group.github.io/}{Wolfgang Kerzendorf}, Depts. of Physics \& Astronomy and CMSE \\
Description: The explosion of academic publications, thanks to the internet, has created a treasure trove of research materials. Yet, this abundance also poses a challenge: staying updated on the most impactful publications and identifying emerging research trends is increasingly difficult. In astronomy alone, around 17,000 new papers are published annually, a number that doubles approximately every 14 years. The NASA Astrophysics Data System (ADS) serves as a comprehensive archive, storing over 16 million publications and their associated metadata (e.g., author names, keywords).  For this project, the selected student will delve into the ADS dataset to analyze emerging trends and areas of focus in astrophysics research, using data science methods such as text mining, machine learning, and data visualization.  By the end of this project, the student will aim to answer the following questions:  What are the most frequently occurring keywords and phrases in astrophysics publications over the past decade? How has this changed over time? Can machine learning models predict which emerging topics are likely to be focal points in future research? How interconnected are various subfields within astrophysics, as indicated by co-authorship and keyword overlap? \\
Representative publications: \cite{kerzendorf1, kerzendorf2}

\vspace{1mm}
\noindent
\textbf{Project \#10: Coarse-Grained Molecular Dynamics Simulation of an Enzyme Complex} (Biocatalysis)\\
Mentor: \href{www.egr.msu.edu/~scb}{Scott Calabrese Barton},  Dept. of Chemical Engineering \& Materials Science \\
Description:  The enzymatic complex hexokinase (HK) -- glucose-6-phosphate dehydrogenase (G6PDH) can accomplish a multistep reaction with strong retention (channeling) of reaction intermediates by electrostatic attraction. However, strong electrostatic charge can influence the structure of the complex over long length and time scales, which in turn can effect the channeling efficiency. In this project, the student researcher will use molecular dynamics combined with coarse graining of the model complex in order to predict the dynamics of complex structure, as well as structural effects on channeling efficiency.  Coarse-grained models simplify the complex by grouping several neighboring atoms into a coarse-grained bead and enables the simulation timescales up to microseconds. These long simulation scales allow the observation of enzymatic configuration change, providing insights that will enable the design of more efficient catalytic complexes. \\
Representative publications: \cite{barton1, barton2}

\vspace{3mm}
\noindent
\textbf{Technical training:} Although students are expected to enter the program with at least one semester of coursework or equivalent or computer programming, the REU students are provided with extensive introductory training on a variety of practical computational skills including introduction to Linux (command-line), Python (programming language), high-performance computing (using MSU's supercomputing cluster), parallel computing, version control (Git), data visualization, and software development best practices.  See Section~\ref{sec:prof_dev} for more details.

\vspace{1mm}
\noindent
\textbf{Professional development:}  Seminars include: Understanding responsible conduct of research, Interacting effectively with a research team, Conducting literature reviews, Framing a research project and writing abstracts, Preparing and delivering a research talk, Creating and presenting posters, Applying to graduate schools, Understanding careers paths, Writing a resume and CV.  See Section~\ref{sec:prof_dev} for more details.

\vspace{1mm}
\noindent
\textbf{Faculty seminar series:}
Each summer will feature four seminars, delivered by MSU faculty, that serve to introduce the students to a broad range of cutting-edge areas of research in computational and data science; and to engage the students in discussions on related topics e.g., life in a faculty led research group, navigating graduate school and postdoctoral appointments in pursuit of an academic career.

\vspace{1mm}
\noindent
\textbf{Creating an interactive and welcoming environment:} The ACRES REU will begin with a ``first day'' seminar and lunch, organized and led by Dr. Dickson and Dr. Parvizi, during which each faculty mentor will meet with their student mentee to welcome them to the research group and discuss expectations for the summer. Specifically, mentors are expected to integrate REU students into the regular activities of their research group, including scheduled one-on-one and group research meetings, and mentees are expected to adhere to group norms regarding work hours and locations. We will provide concrete guidelines to mentors regarding on-boarding students, clearly defining achievable goals and expectations, and giving constructive feedback.  Prior to the student joining the group, mentors are expected to clearly define a student project that will have outcomes achievable in 10 weeks, where the student can demonstrate professional growth and intellectual ownership. They are also required to ensure that the REU students are aware of group norms and expected behavior, individual/group-meeting schedule, graduate students or postdocs in the group that the REU student can go to for help, and the format for giving and receiving feedback. Several deadlines throughout the program will require direct communication between mentors and mentees. Mentors are mandated to participate in the ``first day'' REU seminar, give feedback on REU deliverables, and attend the Mid-SURE poster session and ACRES annual symposium.  A mentor-mentee contract will serve to formalize this discussion to bring both parties to agreement on all these expectations.

%We have implemented several structures that facilitate mentor-mentee interactions.  Students are given explicit expectations regarding participation, deliverables, and managing-up. Specifically, students are expected to actively participate in the life of their host research group (including one-on-one meetings, group meetings, and other activities defined by you) and adhere to group norms about working hours, locations, etc. They are also required to participate in ACRES REU events, including initial training and weekly professional development/seminars. Students also need to achieve a range of deliverables in the form requested and on time (blog posts, mentor-mentee contract, research project-related materials). They are also told about the importance of taking the initiative in working closely with their mentor(s) to make acceptable progress on their research project and actively seek feedback.



In addition to faculty mentors, two current CMSE graduate students will serve as near-peer mentors and play a crucial role in shaping the experience of REU students. These graduate student mentors will meet with the REU students once a week over ACRES group lunches to discuss their integration into the research group, offer professional advice, and serve as general role-models for student researchers.  The near-peer mentors will also serve as the organizers for and leaders of ACRES led social events, including REU wide game/trivia nights and other sponsored outings. %See Section~\ref{sec:prof_dev} for more details.

Finally, students will be provided with many to meet and interact with students from other MSU REU programs. Some example activities will include inter-REU picnics, a tour of FRIB (MSU's Facility for Rare Isotope Beams), an inter-REU trip to Detroit museums and professional baseball games, and an inter-REU trip to the Michigan Adventure amusement park.
