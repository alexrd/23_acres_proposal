\vspace{-3mm}
\section{Project Evaluation and Reporting}
\label{sec:evaluation}
\vspace{-2mm}

% \red{Describe the plan to measure qualitatively and quantitatively the success of the project in achieving its goals, particularly the degree to which students have learned and their perspectives on science, engineering, or education research related to these disciplines have been expanded. Evaluation may involve periodic measures throughout the project to ensure that it is progressing satisfactorily according to the project plan, and may involve pre-project and post-project measures aimed at determining the degree of student learning that has been achieved. In addition, it is highly desirable to have a structured means of tracking participating students beyond graduation, with the aim of gauging the degree to which the REU Site experience has been a lasting influence in the students' career paths. Proposers may wish to consult http://www.evalu-ate.org/resources/doc-2010-nsfhandbook/The 2010 User-Friendly Handbook for Project Evaluation for guidance on the elements in a good evaluation plan. Although not required, REU Site PIs may wish to engage specialists in education research (from their organization or another one) in planning and implementing the project evaluation.}

% \begin{enumerate}
%     \item Reiterate goals of the program (from the overview section, 'objectives')
%     \item Recap the evaluation plan from the 1st iteration of the grant, and then explain how our modified evaluation plan is different.
%     \item Explain pre- and post-project quantitative evaluations (surveys) and how they evaluate our objectives.
%     \item Explain qualitative evaluations (focus groups, near-peer mentor meetings) and how those evaluate our objectives.
%     \item explain how we've been following up with former REU students, and how we will continue to do that in the future.
%     \item develop logic model
% \end{enumerate}

\noindent
\textbf{The goals of the ACRES REU} are to provide students with:

\vspace{-2mm}
\begin{enumerate}
\item Authentic and meaningful research experiences in computational or data science;
  \vspace{-2mm}

\item A general set of technical skills relating to computationally-focused research; and
  \vspace{-2mm}

    \item Professional development that enhances their summer experience, informs them about career and graduate school opportunities, and prepares them to be part of the 21st century workforce.
\end{enumerate}
\vspace{-2mm}

\begin{table}[!htbp]
  \centering
  \caption{
  Evaluation plan for the ACRES program.
  }
  \label{tab:eval}
  \begin{tabular}{p{3.4cm}|p{3.6cm}|p{3.5cm}|p{3.5cm}}
  Goals & Activities & Example Measures & Data Collection \\
  \hline
  \hline
  {\bf Goal 1: Authentic and meaningful research experiences in computational or data science} & \makecell{- Research environment \\ - Lab meetings \& \\ social activities \\ - Near-peer meetings \\ - Student presentations \\ - Conference attend-\\ance post-REU} & \makecell{- Student participation \\ - Student reflections \\ - No. of student conf- \\erence  presentations} & \makecell{- Pre-post CISE \\ A la Carte Survey \\ - Mentor survey \\ - Qualitative interviews \\ - Longitudinal data \\ - Student blog posts \\ - Near-peer meetings} \\
  \hline
  {\bf Goal 2: Development of technical skills relating to computationally-focused research} & \makecell{- Research environment \\ - Skills workshops \\ - Research seminars} & \makecell{- CISE indicators: \\ self-efficacy, research \\ skills, academic help-\\seeking/coping skills \\ - Student confidence\\ in computing tasks \\(ACRES survey)} & \makecell{- Pre-post CISE \\ A la Carte Survey\\ - Mentor survey \\ - ACRES skills survey \\- Qualitative interviews\\- Longitudinal data} \\
  \hline
  {\bf Goal 3: Professional development that shapes their career and prepares them to be part of a 21st century workforce} & \makecell{- Professional devel-\\opment talk series \\ - Student presentations \\ - Near-peer meetings} & \makecell{- CISE indicators: \\ intent to attend grad-\\uate school, attitudes\\ towards computing \\ disciplines, scientific \\identity \\- Student reflections} & \makecell{-	Pre-post CISE \\ A la Carte Survey \\ - Qualitative interviews} \\
\end{tabular}
\end{table}

The planned activities that seek to ensure these outcomes (described in detail in Sections~\ref{sec:nature} and~\ref{sec:prof_dev}) are given in the Evaluation Plan below (Table \ref{tab:eval}).  The table also includes specific example measures used to measure the achievement of each goal (``Example Measures''), as well as the specific instruments used to collect the data (``Data Collection'').
This set of assessments, goals and activities has been refined through the experience gained in previous iterations of the REU.
The plan presented below makes good use of the tools provided by the CISE REU Toolkit, which streamlines implementation, as well as facilitates centralized data collection for the CISE REU program.
These are supplemented by some evaluation instruments developed at MSU that we have previously found useful for tracking student gains, monitoring the quality of the REU experience, and soliciting information that can be used to improve the program.

\vspace{1mm}
\noindent
\textbf{Quantitative Pre- and post-program assessments.} Students participating in the ACRES REU program will complete the CISE REU A La Carte Survey \cite{CISE_A_LA_CARTE} designed to evaluate the effectiveness of CISE REU programs. The evaluation is administered by the CISE REU Assessment Work Group (NSF award \#1346847) and ``measures student outcomes using a repeated measures design i.e. pre-test, post-test evaluation conducted at the beginning and end of the REU program. The indicator constructs measured were: self-efficacy, intent to attend graduate school, attitudes towards computing disciplines, academic help-seeking/coping skills, research skills, leadership skills, and scientific identity'' \cite{Rorrer2016}.
The specific indicators used to evaluate each of the three goals are given in Table \ref{tab:eval}.

Students will also complete a computational skills survey designed to evaluate the impact of the ACRES program on students technical learning. This survey was developed by the CEDER group (University of Michigan) in consultation with the ACRES team and measures outcomes using a repeated measures design. In the past, it has been administered together with the CISE REU A La Carte Survey. The surveys ask students to evaluate their confidence and gains in performing the following tasks: writing basic shell scripts, applying best practices for developing scientific software, scripting in a programming language of their choice, using a computing cluster (or supercomputer) - including accessing installed software and submitting jobs, developing parallel codes for modern supercomputers, and creating meaningful data visualizations for a given data set. 

\vspace{1mm}
\noindent
\textbf{Qualitative Pre- and post-program assessments.} A post-program qualitative evaluation will be administered to students as an hour-long focus group on the last day of the program. This debrief aims to capture students' experience with different components of the REU program and understand how they can be improved. Additionally, it allows students to reflect more deeply on their gains from the experience in the ACRES REU program and the impact the experience might have on their career plans and next steps once they return to campus. To minimize the potential for investigator bias, we have arranged for an internal collaborator (Prof. Marcos (Danny) Caballero) to conduct these interviews, who is otherwise not involved in the program leadership. Prof. Caballero is a professor of physics and computational science who conducts research in computing education using both quantitative and qualitative methods. He has been awarded several NSF grants to conduct research and development in computing education (e.g., DUE-15044786, DUE-1524128, and DRL-1741575) and was recently elected an American Physical Society fellow for his contributions to the efforts to integrate computing into physics education.

For mentors, we will administer a survey in the week after the REU ends using the Qualtrics platform \cite{qualtrics}. The survey is administered to all research mentors, including faculty, graduate students, and postdocs. It aims to understand the various mentoring structures that students are exposed to in the REU, each person’s role in mentoring students as well as opportunities the experience provides for training and development of graduate students. Additionally, the survey asks mentors for feedback on their experience as mentors. More specifically, mentors comment on the effectiveness of, and ways to improve, recruitment, student preparation for their projects, and communication with the ACRES leadership team. Finally, mentors provide feedback on the research outcomes of their work with ACRES participants over the summer, including their observations of students gains in their domain knowledge, computational skills and ability to think and operate like scientists.

\vspace{1mm}
\noindent
\textbf{Formative assessments.} The program contains several elements that act as formative evaluation tools.  These include student self-evaluations administered immediately following technical and professional development activities, regular PI and near-peer check-in meetings with students and mentors, and weekly student updates via blog posts.
